﻿//////////////////////////////////////////
//该版备注：
// 1.宏定义一个 our_test为uint8_t 或 bool，调试时
// 
///////////////////////////////////////////
#include "image.h"
#include <iostream>
#include"math.h"

#define gap 4
#define pi 3.14

//#define cross_road_flag 0
//#define circle_flag_stage_one 1
//#define curve_flag 2
//#define straight_way_flag 3
//#define near_cross_road_flag 4
//#define circle_flag_stage_two 5
//#define circle_flag_stage_three 6
//#define circle_flag_stage_four 7
//#define circle_flag_stage_five 8
//#define circle_flag_stage_six 9
//#define circle_flag_stage_seven 10
//#define circle_flag_stage_eight 11
//#define circle_flag_stage_nine 12
//#define hill_flag 13
//#define mid_cross_road_flag 14

#define cut_line 27
#define num_all ((120-cut_line)*188)
#define MID_MAX 200
//赵老板复制来的，反正不怕多

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int f[30 * CAMERA_H];//考察连通域联通性
//每个白条子属性
typedef struct {
	uint8_t   left;//左边界
	uint8_t   right;//右边界
	int   connect_num;//连通标记
}range;

//每行的所有白条子
typedef struct {
	uint8_t   num;//每行白条数量
	range   area[white_num_MAX];//该行各白条区域
}all_range;

//属于赛道的每个白条子属性
typedef struct {
	uint8_t   left;//左边界
	uint8_t   right;//右边界
	uint8_t   width;//宽度
}road_range;

//每行属于赛道的每个白条子
typedef struct {
	uint8_t   white_num;
	road_range   connected[white_num_MAX];
}road;

typedef struct {
	double   i;
	double   j;
}mid;
mid mid_float[MID_MAX];

all_range white_range[CAMERA_H];//所有白条子
all_range black_range[CAMERA_H];//所有黑条子
road my_road[CAMERA_H];//赛道
our_test IMG[CAMERA_H][CAMERA_W];//二值化图像数组
uint8_t my_road_num[CAMERA_H];
uint8_t x_left[CAMERA_H], x_right[CAMERA_H];
uint8_t left_line[CAMERA_H], right_line[CAMERA_H];//赛道的左右边界
uint8_t left_smooth[CAMERA_H], right_smooth[CAMERA_H];
uint8_t mid_line[CAMERA_H];
int all_connect_num = 0;//所有白条子数
uint8_t top_road;//赛道最高处所在行数

int my_connect_num = 0;
int black_area = 0;

//以上为赛道基本元素的定义
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//与控制交接量的定义

uint8_t threshold = 150;//阈值
//float my_angle = 0;//定义与前瞻的偏差值
uint8_t PROSPECT = 70;//定义前瞻的半径值
//uint8_t road_flag = 0;//定义赛道类型识别标志量

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//各种标志量的定义

uint8_t img_protect = 0;//定义出赛道标志量
uint8_t my_fork = 0;//识别岔道标志量
uint8_t fork_flag = 0;//岔路识别辅助标志量
uint8_t fork_test = 0;//岔路识别辅助标志量
uint8_t my_straight = 0;//识别直道标志量

//十字
uint8_t my_cross = 0;//识别十字标志量
uint8_t cross_outL = MISS;
uint8_t cross_outR = MISS;
uint8_t cross_enterL = MISS;
uint8_t cross_enterR = MISS;

uint8_t white_through = MISS;//定义最近的贯穿白条位置

//岔路
uint8_t fork_breakL = 0;//left broken point
uint8_t fork_breakR = 0;//right broken point

//斑马线和车库
uint8_t zebra_start_line = MISS;//定义识别斑马线标志量
uint8_t zebra_end_line = MISS;//定义斑马线的出口
uint8_t my_garage = 1;//识别出入库的标志量
uint8_t garage_dir = MISS;//车库方向(0为左侧，1为右侧)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//辛老板环岛定义量

uint8_t left_break_down[2], right_break_down[2];
uint8_t left_break_up[2], right_break_up[2];

uint8_t road_case_1 = 1; //左边直线，右下方是直线段，无上断点，有C型弧线
uint8_t road_case_2 = 2;//左边直线，右下方无线段，无上端点，有C型弧线
uint8_t road_case_3 = 3;//左边直线，右下方无线段，有上端点，有C型弧线
uint8_t road_case_4 = 4;//左边直线，右下方C型弧线段，有上端点
uint8_t road_case_5 = 5;//左边直线，右下方C型弧线段，无上端点

uint8_t break_point_on_left[6];
uint8_t break_point_on_right[6];
uint8_t how_many_break_on_right;
uint8_t how_many_break_on_left;

uint8_t akward_area;
uint8_t tell_all_white_flag;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//杂七杂八的定义量

#if 1
uint8_t left_side[CAMERA_H] = { 0 };
uint8_t right_side[CAMERA_H] = { CAMERA_W - 1 };
#endif

float parameterA = 0, parameterB = 0;
bool x_left_straight = 0;
bool x_right_straight = 0;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//动态阈值及二值化

uint8_t HistGram[256];
void get_histgram(void);
void GetOSTUThreshold(uint8_t* HistGram);
////////////////////////////////////////////
//功能：二值化
//输入：灰度图片
//输出：二值化图片
//备注：
///////////////////////////////////////////
void THRE()
{
	//get_histgram();
	//GetOSTUThreshold(HistGram);
	threshold = 120;
	std::cout << unsigned(threshold) << std::endl;
	
	uint8_t* map;
	//uint8_t* my_map;
	our_test temp = 0;
	map = fullBuffer;
	for (uint8_t i = 0; i < CAMERA_H; i++)
	{
		//my_map = &IMG[i][0];
		for (uint8_t j = 0; j <= CAMERA_W; j++)
		{

			if (j == 0)
			{
				if ((*map) > threshold)
					temp = 1;
				else
					temp = 0;
				map++;
			}
			else if (j == CAMERA_W)
			{
				*(map - 1) = temp;
			}
			else
			{
				*(map - 1) = temp;
				if ((*map) > threshold)
					temp = 1;
				else
					temp = 0;
				map++;
			}
			/*
			if ((*map) > threshold)
				(*my_map) = 1;
			else (*my_map) = 0;
			map++;
			my_map++;
			*/
		}
	}
}
void get_histgram()
{
	uint8_t* map;
	double parameter = 100000.0;
	map = fullBuffer;
	uint8_t temp = 0;
	uint8_t* hist_head = &HistGram[0];
	while (*map)
	{
		*(hist_head + *map) = *(hist_head + *map) + 1;
		map++;
	}
	for (int i = 0; i <= 255; i++)
	{
		if (*(hist_head + i))
		{
			if (i == 0)
				temp = (*(hist_head + i) / (parameter)) * 10000;
			else if (i == 255)
			{
				*(hist_head + i - 1) = temp;
				*(hist_head + i) = (*(hist_head + i) / (parameter)) * 10000;
			}
			else
			{
				*(hist_head + i - 1) = temp;
				temp = (*(hist_head + i) / (parameter)) * 10000;
			}
		}
	}
}
void  GetOSTUThreshold(uint8_t* HistGram)
{
	int X, Y, Amount = 0;
	int PixelBack = 0, PixelFore = 0, PixelIntegralBack = 0, PixelIntegralFore = 0, PixelIntegral = 0;
	double OmegaBack, OmegaFore, MicroBack, MicroFore, SigmaB, Sigma;              // 类间方差;
	int MinValue, MaxValue;
	uint8_t Threshold = 0;

	for (MinValue = 0; MinValue < 256 && HistGram[MinValue] == 0; MinValue++);
	for (MaxValue = 255; MaxValue > MinValue && HistGram[MinValue] == 0; MaxValue--);

	for (Y = MinValue; Y <= MaxValue; Y++) Amount += HistGram[Y];        //  像素总数

	PixelIntegral = 0;
	for (Y = MinValue; Y <= MaxValue; Y++) PixelIntegral += HistGram[Y] * Y;
	SigmaB = -1;
	for (Y = MinValue; Y < MaxValue; Y++)
	{
		PixelBack = PixelBack + HistGram[Y];
		PixelFore = Amount - PixelBack;
		OmegaBack = (double)PixelBack / Amount;
		OmegaFore = (double)PixelFore / Amount;
		PixelIntegralBack += HistGram[Y] * Y;
		PixelIntegralFore = PixelIntegral - PixelIntegralBack;
		MicroBack = (double)PixelIntegralBack / PixelBack;
		MicroFore = (double)PixelIntegralFore / PixelFore;
		Sigma = OmegaBack * OmegaFore * (MicroBack - MicroFore) * (MicroBack - MicroFore);
		if (Sigma > SigmaB)
		{
			SigmaB = Sigma;
			Threshold = (uint8_t)Y;
		}
	}
	threshold = Threshold;
}

void head_clear(void)
{
	our_test* my_map;
	for (int i = 99; i >= 70; i--)
	{
		my_map = &IMG[i][0];
		for (int j = 30; j <= 100; j++)
		{
			*(my_map + j) = white;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////
//功能：逆透视变换 // 指针优化版
//输入：
//输出：
//备注：无
///////////////////////////////////////////
//void map(void)
//{
//	int i;
//	int j;
//	uint8_t* pp = NULL;
//	uint8_t* ss = NULL;
//	pp = &left_side[0];
//	ss = &right_side[0];
//	uint8_t* str = NULL;
//	uint8_t* ptr = NULL;
//	str = &IMG[0][0];//变换后数组
//	ptr = &temp_IMG[0][0];//变换前数组
//	uint8_t* pptr = NULL;
//	uint8_t* sstr = NULL;
//	pptr = &i_fix[0];
//	sstr = &j_fix[0][0];
//	int j_begin, j_end;
//	uint8_t* tstr = NULL;
//	uint8_t* tsstr = NULL;
//	uint8_t* tptr = NULL;
//	for (i = 0; i < CAMERA_H; i++)
//	{
//		j_begin = *(pp + i);
//		j_end = *(ss + i);
//
//		tstr = str + i * 188;
//		tsstr = sstr + i * 188;
//		tptr = ptr + *(pptr + i) * 188;
//		for (j = j_begin; j <= j_end; j++)
//		{
//			*(tstr + j) = *(tptr + *(tsstr + j));
//			//IMG[i][j]= temp_IMG[*(pptr + i)][*(tsstr + j)];
//		}
//	}
//
//}//map

////////////////////////////////////////////
//功能：查找父节点
//输入：节点编号
//输出：最老祖先
//备注：含路径压缩
///////////////////////////////////////////
int find_f(int node)
{
	if (f[node] == node)return node;//找到最古老祖先，return
	f[node] = find_f(f[node]);//向上寻找自己的父节点
	return f[node];
}

////////////////////////////////////////////
//功能：提取跳变沿 并对全部白条子标号
//输入：IMG[120][188]
//输出：white_range[120]
//备注：指针提速
///////////////////////////////////////////
void search_white_range()
{
	uint8_t i, j;
	int istart = NEAR_LINE;//处理起始行
	int iend = FAR_LINE;//处理终止行
	int tnum = 0;//当前行白条数
	all_connect_num = 0;//白条编号初始化
	our_test* map = NULL;
	for (i = istart; i >= iend; i--)
	{
		map = &IMG[i][LEFT_SIDE];//指针行走加快访问速度
		tnum = 0;
		for (j = LEFT_SIDE; j <= RIGHT_SIDE; j++, map++)
		{
			if ((*map))//遇白条左边界
			{
				tnum++;
				if (tnum >= white_num_MAX)break;
				range* now_white = &white_range[i].area[tnum];
				now_white->left = j;

				//开始向后一个一个像素点找这个白条右边界
				map++;
				j++;

				while ((*map) && j <= RIGHT_SIDE)
				{
					map++;
					j++;
				}
				now_white->right = j - 1;
				now_white->connect_num = ++all_connect_num;//白条数加一，给这个白条编号
			}
		}
		white_range[i].num = tnum;
	}
}

////////////////////////////////////////////
//功能：寻找白条子连通性，将全部联通白条子的节点编号刷成最古老祖先的节点编号
//输入：
//输出：
//备注：
///////////////////////////////////////////
void find_all_connect()
{
	//f数组初始化
	for (int i = 1; i <= all_connect_num; i++)
		f[i] = i;

	//u为up d为down 即为当前处理的这两行中的上面那行和下面那行
	//u_num：上面行白条数
	//u_left：上面行当前白条左边界
	//u_right：上面行当前白条右边界
	//i_u：当前处理的这个白条是当前这行（上面行）白条中的第i_u个
	int u_num, i_u, u_left, u_right;
	int d_num, i_d, d_left, d_right;
	all_range* u_white = NULL;
	all_range* d_white = NULL;
	for (int i = NEAR_LINE; i > FAR_LINE; i--)//因为每两行每两行比较 所以循环到FAR_LINE+1
	{
		u_num = white_range[i - 1].num;
		d_num = white_range[i].num;
		u_white = &white_range[i - 1];
		d_white = &white_range[i];
		i_u = 1; i_d = 1;

		//循环到当前行或上面行白条子数耗尽为止
		while (i_u <= u_num && i_d <= d_num)
		{
			//变量先保存，避免下面访问写的冗杂且访问效率低
			u_left = u_white->area[i_u].left;
			u_right = u_white->area[i_u].right;
			d_left = d_white->area[i_d].left;
			d_right = d_white->area[i_d].right;

			if (u_left <= d_right && u_right >= d_left)//如果两个白条联通
				f[find_f(u_white->area[i_u].connect_num)] = find_f(d_white->area[i_d].connect_num);//父节点连起来

			//当前算法规则，手推一下你就知道为啥这样了
			if (d_right > u_right)i_u++;
			if (d_right < u_right)i_d++;
			if (d_right == u_right) { i_u++; i_d++; }
		}
	}
}

////////////////////////////////////////////
//功能：寻找赛道
//输入：
//输出：
//备注：
///////////////////////////////////////////
void find_road()
{
	uint8_t istart = NEAR_LINE;
	uint8_t iend = FAR_LINE;
	top_road = NEAR_LINE;//赛道最高处所在行数，先初始化话为最低处
	int road_f = -1;//赛道所在连通域父节点编号，先初始化为-1，以判断是否找到赛道
	int while_range_num = 0, roud_while_range_num = 0;
	all_range* twhite_range = NULL;
	road* tmy_road = NULL;
	//寻找赛道所在连通域
	// 寻找最中心的白条子
	for (int i = 1; i <= white_range[istart].num; i++)
		if (white_range[istart].area[i].left <= CAMERA_W / 2
			&& white_range[istart].area[i].right >= CAMERA_W / 2 && (white_range[istart].area[i].right - white_range[istart].area[i].left) >= 90)
			road_f = find_f(white_range[istart].area[i].connect_num);

	if (road_f == -1)//若赛道没在中间，在113行选一行最长的认为这就是赛道
	{
		int widthmax = 0, jselect = 1;
		for (int i = 1; i <= white_range[istart].num; i++)
			if (white_range[istart].area[i].right - white_range[istart].area[i].left > widthmax)
			{
				widthmax = white_range[istart].area[i].right - white_range[istart].area[i].left;
				jselect = i;
			}
		road_f = find_f(white_range[istart].area[jselect].connect_num);
	}

	//现在我们已经得到了赛道所在连通域父节点编号，接下来把所有父节点编号是road_f的所有白条子扔进赛道数组就行了
	for (int i = istart; i >= iend; i--)
	{
		//变量保存，避免之后写的冗杂且低效
		twhite_range = &white_range[i];
		tmy_road = &my_road[i];
		while_range_num = twhite_range->num;
		tmy_road->white_num = 0;
		roud_while_range_num = 0;
		for (int j = 1; j <= while_range_num; j++)
		{
			if (find_f(twhite_range->area[j].connect_num) == road_f)
			{
				top_road = i;
				tmy_road->white_num++; roud_while_range_num++;
				tmy_road->connected[roud_while_range_num].left = twhite_range->area[j].left;
				tmy_road->connected[roud_while_range_num].right = twhite_range->area[j].right;
				tmy_road->connected[roud_while_range_num].width = twhite_range->area[j].right - twhite_range->area[j].left;

			}
		}
	}
}

////////////////////////////////////////////
//功能：返回相连下一行白条子编号
//输入：i_start起始行  j_start白条标号
//输出：白条标号
//备注：认为下一行与本行赛道重叠部分对多的白条为选定赛道
///////////////////////////////////////////
uint8_t find_continue(uint8_t i_start, uint8_t j_start)
{
	uint8_t j_return;
	uint8_t j;
	uint8_t width_max = 0;
	uint8_t width_new = 0;
	uint8_t left = 0;
	uint8_t right = 0;
	uint8_t dright, dleft, uright, uleft;
	j_return = MISS;//如果没找到，输出255
	if (j_start > my_road[i_start].white_num)
		return MISS;
	//选一个重叠最大的
	for (j = 1; j <= my_road[i_start - 1].white_num; j++)
	{
		dleft = my_road[i_start].connected[j_start].left;
		dright = my_road[i_start].connected[j_start].right;
		uleft = my_road[i_start - 1].connected[j].left;
		uright = my_road[i_start - 1].connected[j].right;
		if (//相连
			dleft < uright
			&&
			dright > uleft
			)
		{
			//计算重叠大小
			if (dleft < uleft) left = uleft;
			else left = dleft;

			if (dright > uright) right = uright;
			else right = dright;

			width_new = right - left + 1;

			if (width_new > width_max)
			{
				width_max = width_new;
				j_return = j;
			}
		}

	}
	return j_return;
}

//////////////////////////////////////////////
////功能：寻找当前行最可能是我要行进赛道的白条编号
////输入：行数
////输出：白条编号
////备注：无
/////////////////////////////////////////////
uint8_t define_my_way(uint8_t line)
{
	road* tmy_road = NULL;
	uint8_t  my_way_num;
	uint8_t distance = CAMERA_W;
	my_way_num = 0;
	tmy_road = &my_road[line];
	uint8_t max_way = 0, max_way_num = 0;
	uint8_t last_line;
	last_line = last_mid_line[line];
	if (my_road[line].white_num >= 2)
	{
		/*for (uint8_t j = 1; j <= tmy_road->white_num; j++)
		{
			if (tmy_road->connected[j].left <= last_mid_line[line] + 3 && tmy_road->connected[j].right >= last_mid_line[line] - 3)
			{
				if (tmy_road->connected[j].width > 15)
				{
					my_way_num = j;
					break;
				}
			}
		}*/
		//2021.3.15修改

		for (uint8_t j = 1; j <= tmy_road->white_num; j++)
		{
			if (tmy_road->connected[j].left <= mid_line[line] + 3 && tmy_road->connected[j].right >= mid_line[line] - 3)
			{
				if (tmy_road->connected[j].width > 15)
				{
					my_way_num = j;
					break;
				}
			}
		}

		/*for (uint8_t j = 1; j <= tmy_road->white_num; j++)
		{
			if (My_Abs((tmy_road->connected[j].left + tmy_road->connected[j].right) - CAMERA_W) <= 20)
			{
				if (tmy_road->connected[j].width > 15)
				{
					my_way_num = j;
					break;
				}
			}
		}*/

		if (my_way_num == 0)
		{
			for (uint8_t j = 1; j <= tmy_road->white_num; j++)
			{
				if (tmy_road->connected[j].width >= max_way)
				{
					max_way = tmy_road->connected[j].width;
					max_way_num = j;
				}
				my_way_num = max_way_num;
				/*if (abs(last_mid_line[line] - (tmy_road->connected[j].left + tmy_road->connected[j].right) / 2) < distance)
					{
						distance = abs(last_mid_line[line] - (tmy_road->connected[j].left + tmy_road->connected[j].right) / 2);
						my_way_num = j;
					}*/
			}
		}
	}
	else if (my_road[line].white_num == 1)
	{
		my_way_num = 1;
	}
	else my_way_num = 0;
	return my_way_num;
}

////////////////////////////////////////////
//功能：寻找赛道
//输入：
//输出：
//备注：
///////////////////////////////////////////
void find_x_right_left(void)
{
	uint8_t num;
	uint8_t cut_flag = 0;
	uint8_t i_start, i_end;
	i_start = NEAR_LINE;
	i_end = FAR_LINE;
	memset(my_road_num, MISS, CAMERA_H);

	for (uint8_t i = i_start; i >= i_end; i--)
	{
		my_road_num[i] = define_my_way(i);
		//printf("my_road_num[%d]:%d\n", i, my_road_num[i]);
	}

	for (uint8_t i = i_start; i >= i_end; i--)
	{
		if (my_road_num[i] == 0)
		{
			x_left[i] = MISS;
			x_right[i] = MISS;
		}
		else
		{
			x_left[i] = my_road[i].connected[my_road_num[i]].left;
			x_right[i] = my_road[i].connected[my_road_num[i]].right;
			//printf("x_left[%d]:%d,x_right[%d]:%d\n", i, x_left[i], i, x_right[i]);
		}

	}
	//连通判断
	uint8_t break_line = MISS;
	for (uint8_t i = 70; i >= 1; i--)
	{
		if ((x_left[i + 1] >= x_right[i] || x_right[i + 1] <= x_left[i]) && x_left[i] != MISS)
		{
			break_line = i;
			break;
		}
	}
	//printf("break_line:%d", break_line);
	if (break_line != MISS)
		for (uint8_t i = break_line; i > 1; i--)
		{
			cut_flag = 0;
			num = my_road[i].white_num;
			if (num == 0) break;
			for (uint8_t j = 1; j <= num; j++)
			{
				if (my_road[i].connected[j].left <= x_right[i + 1] && my_road[i].connected[j].right >= x_left[i + 1])
				{
					x_left[i] = my_road[i].connected[j].left;
					x_right[i] = my_road[i].connected[j].right;
					//printf("x_left[%d]:%d,x_right[%d]:%d\n", i, x_left[i], i, x_right[i]);
					break_line = i - 1;
					cut_flag = 1;
				}
			}
			if (cut_flag == 0) break;
		}
	if (break_line != MISS)
	{
		for (uint8_t i = break_line; i > 1; i--)
		{
			x_left[i] = MISS;
			x_right[i] = MISS;
		}
	}
}

/*////////////////////////////////////////////
//绝对值
*/////////////////////////////////////////////
int My_Abs(signed int i)
{
	if (i < 0)
		return ~(--i);
	return i;
}

////////////////////////////////////////////
//功能：确定是不是直道
//输入：数组指针，起始行，终止行
//输出：0---不直  1---直  
//备注：方差50为直道的分界线，从学长那里copy的
//日期：2021.1.29
///////////////////////////////////////////
bool define_straight_line(uint8_t* line, uint8_t start_line, uint8_t end_line)
{
	uint8_t i_start, i_end;
	uint8_t i;
	for (i = end_line; i <= 15; i++)
	{
		if (*(line + i) != MISS)  break;
	}
	if (i == 15) return 0;
	i_start = start_line;
	i_end = i;

	int x1, x2;
	int y1, y2;
	int delta_x, delta_y;
	float k, b;
	int output, add_square = 0;
	float variance;

	x1 = i_start;
	x2 = i;
	y1 = *(line + i_start);
	y2 = *(line + i);

	delta_x = x1 - x2;
	delta_y = y1 - y2;
	k = (float)delta_y / (float)delta_x;
	b = y1 - (float)x1 * k;

	for (i = i_start; i >= i_end; i--)
	{
		output = k * i + b; //算
		output = output - *(line + i);//差
		output *= output;//平方
		add_square = add_square + output;//累加
	}

	variance = (float)add_square / (float)(i_start - i_end);//方差
	if (variance <= 5) return 1;
	else return 0;
}

////////////////////////////////////////////
//功能：通用决定双边
//输入：
//输出：
//备注：
///////////////////////////////////////////
void ordinary_two_line(void)
{
	uint8_t i;
	uint8_t j;
	uint8_t j_continue[CAMERA_H];//第一条连通路径
	uint8_t i_start;
	uint8_t i_end;
	uint8_t j_start = MISS;
	int width_max;

	//寻找起始行最宽的白条子
	i_start = NEAR_LINE;
	i_end = FAR_LINE;
	width_max = 0;
	for (j = 1; j <= my_road[i_start].white_num; j++)
	{
		if (my_road[i_start].connected[j].width > width_max)
		{
			width_max = my_road[i_start].connected[j].width;
			j_start = j;
		}
	}
	j_continue[i_start] = j_start;

	//记录连贯区域编号
	for (i = i_start; i > i_end; i--)
	{
		//如果相连编号大于该行白条数，非正常，从此之后都MISS
		if (j_continue[i] > my_road[i].white_num)
		{
			j_continue[i - 1] = MISS;
		}
		else
		{
			j_continue[i - 1] = find_continue(i, j_continue[i]);
		}

	}

	//全部初始化为MISS
	my_memset(left_line, MISS, CAMERA_H);
	my_memset(right_line, MISS, CAMERA_H);


	for (i = i_start; i > i_end; i--)
	{
		if (j_continue[i] <= my_road[i].white_num)
		{
			left_line[i] = my_road[i].connected[j_continue[i]].left;
			right_line[i] = my_road[i].connected[j_continue[i]].right;
			IMG[i][left_line[i]] = blue;
			IMG[i][right_line[i]] = red;
		}
		else
		{
			left_line[i] = MISS;
			right_line[i] = MISS;
		}
	}
}

////////////////////////////////////////////
//功能：直道决定双边
//输入：
//输出：
//备注：
///////////////////////////////////////////
void straight_oridinary_two_line(void)
{
	uint8_t way_num;
	//uint8_t last_l_line;
	//uint8_t last_r_line;

	uint8_t* l_ptr = NULL;
	uint8_t* r_ptr = NULL;

	uint8_t* l_line_ptr = NULL;
	uint8_t* r_line_ptr = NULL;

	l_line_ptr = &left_line[0];
	r_line_ptr = &right_line[0];

	uint8_t* l_line_pptr = NULL;
	uint8_t* r_line_pptr = NULL;
	for (uint8_t i = 113; i >= 1; i--)
	{
		way_num = define_my_way(i);
		if (way_num != 0)
		{
			l_ptr = &my_road[i].connected[way_num].left;
			r_ptr = &my_road[i].connected[way_num].right;

			l_line_pptr = l_line_ptr + i;
			r_line_pptr = r_line_ptr + i;

			*l_line_pptr = *l_ptr;
			*r_line_pptr = *r_ptr;
			/*if (i <= 112)
			{
				last_l_line = *(l_line_pptr + 1);
				last_r_line = *(r_line_pptr + 1);

				if (*l_line_pptr - last_l_line >= 2)
					*l_line_pptr = last_l_line + 2;
				else if (*l_line_pptr - last_l_line <= -2)
					*l_line_pptr = last_l_line - 2;

				if (*r_line_pptr - last_r_line >= 2)
					*r_line_pptr = last_r_line + 2;
				else if(*r_line_pptr - last_r_line <= -2)
					*r_line_pptr = last_r_line - 2;
			}*/
		}
		else
		{
			left_line[i] = MISS;
			right_line[i] = MISS;
		}

	}
	return;
}

////////////////////////////////////////////
//功能：十字识别,顺便找出入口
//输入：
//输出：0 不是十字   1 是第一型十字 2是第二型十字
//备注：
///////////////////////////////////////////
uint8_t check_cross(void)
{
	uint8_t i_start, i_end;
	i_start = NEAR_LINE;
	i_end = FAR_LINE;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	uint8_t left_num = 0;
	uint8_t right_num = 0;
	uint8_t tmy_road_num = 0;
	for (uint8_t m = FAR_LINE; m <= FAR_LINE + 10; m++)
	{
		//printf("x_left[%d]:%d,x_right[%d]:%d,my_road[%d].width:%d\n", m, x_left[m], m, x_right[m], m, my_road[m].connected[my_road_num[m]].width);
		if ((x_left[m] <= 30 || x_right[m] <= CAMERA_W /2) && my_road[m].connected[my_road_num[m]].width <= 30)
		{
			left_num++;
		}
		if ((x_right[m] >= CAMERA_W - 30 || x_left[m] >= CAMERA_W / 2) && my_road[m].connected[my_road_num[m]].width <= 30)
			right_num++;
		if (my_road_num[m] == 0)
			tmy_road_num++;
	}
	if (left_num >= 5)
	{
		printf("上端道路过多靠左线，十字不通过！\n");
		return 0;
	}
	if (right_num >= 5)
	{
		printf("上端道路过多靠右线，十字不通过！\n");
		return 0;
	}
	if (tmy_road_num >= 5)
	{
		printf("MISS行过多，十字不通过！\n");
		return 0;
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////



	int cosL[CHANGED_H] = { 0 };//余弦的平方左 扩大1000倍
	int cosR[CHANGED_H] = { 0 };//余弦的平方右 扩大1000倍
	int x1 = 0;
	int x2 = 0;
	int y2 = 0;

	y2 = gap * gap;

	cross_outL = MISS;
	cross_outR = MISS;
	cross_enterL = MISS;
	cross_enterR = MISS;

	fork_breakL = MISS;
	fork_breakR = MISS;

	uint8_t cross_flag = MISS;

	uint8_t l_enter = MISS;
	uint8_t r_enter = MISS;

	uint8_t l_out = MISS;
	uint8_t r_out = MISS;

	//十字的参数
	int cross_parameter1 = 500;//cos的值的阈值
	uint8_t cross_parameter2 = 8;//x_left和x_right与其自身的差值
	uint8_t cross_parameter3 = 15;//允许的中央白条离边界的距离

	//岔路的参数
	int fork_parameter1 = 500;//cos的值的阈值
	uint8_t fork_parameter2 = 10;//x_left和x_right与其自身的差值

	for (uint8_t i = i_start; i >= (i_end + gap); i--)
	{
		//计算左侧余弦值的平方
		x1 = (int)x_left[i - gap] - (int)x_left[i];
		x2 = (int)x_left[i] - (int)x_left[i + gap];
		cosL[i] = x1 * x2 + y2;
		cosL[i] *= cosL[i];
		cosL[i] = cosL[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));

		//计算右侧余弦值平方
		x1 = (int)x_right[i - gap] - (int)x_right[i];
		x2 = (int)x_right[i] - (int)x_right[i + gap];
		cosR[i] = x1 * x2 + y2;
		cosR[i] *= cosR[i];
		cosR[i] = cosR[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));
		//printf("行数：%d 左余弦：%d 右余弦：%d\n",i, cosL[i], cosR[i]);
		//printf("行数：%d 左线：%d 右线：%d 左余弦：%d 右余弦：%d\n", i, x_left[i], x_right[i], cosL[i], cosR[i]);
	}
	for (uint8_t j = (i_start - gap); j >= (i_end + gap*gap); j--)
	{
		if (cosL[j] <= fork_parameter1 && cosL[j] >= 0 && (x_left[j] - x_left[j - gap] >= fork_parameter2) && fork_breakL == MISS)
		{
			fork_breakL = j;
		}
		if (cosL[j] <= cross_parameter1 && cosL[j] >= 0 && (x_left[j] - x_left[j - gap] >= cross_parameter2))
		{
			cross_enterL = j;
			break;
		}
	}
	for (uint8_t j = (i_start - gap); j >= (i_end + gap*gap); j--)
	{
		if (cosR[j] <= fork_parameter1 && cosR[j] >= 0 && (x_right[j - gap] - x_right[j] >= fork_parameter2) && fork_breakR == MISS)
		{
			fork_breakR = j;
		}
		if (cosR[j] <= cross_parameter1 && cosR[j] >= 0 && (x_right[j - gap] - x_right[j] >= cross_parameter2))
		{
			cross_enterR = j;
			break;
		}
	}

	for (uint8_t i = (i_start - 3); i >= (i_end + gap); i--)
	{
		// printf("行数：%d,l_enter:%d\n", i, My_Abs(x_left[i - gap] - left_side[i - gap]));
		if (My_Abs(x_left[i - gap] - left_side[i - gap]) <= cross_parameter3)
		{
			l_enter = i;
			break;
		}
	}
	for (uint8_t i = (i_start - 3); i >= (i_end + gap); i--)
	{
		//printf("x_right[%d]:%d;right_side[%d]:%d\n", i, x_right[i],i,right_side[i]);
		//printf("行数：%d,r_enter:%d\n", i, My_Abs(x_right[i - gap] - right_side[i - gap]));
		if (My_Abs(x_right[i - gap] - right_side[i - gap]) <= cross_parameter3)
		{
			r_enter = i;
			break;
		}
	}
	if (l_enter == MISS || r_enter == MISS)
	{
		printf("l_enter:%d,r_enter:%d\n", l_enter, r_enter);
		printf("无可用中央白条子\n");
	}
	printf("左进口在：%d，右进口在%d\n", cross_enterL, cross_enterR);
	if (My_Abs(cross_enterL - cross_enterR) <= 20 && cross_enterL != MISS && cross_enterR != MISS && l_enter != MISS && r_enter != MISS)
	{
		for (uint8_t j = (cross_enterL - gap); j >= (i_end + 2); j--)
		{
			//printf("我找点了！！\n");
			if (cosL[j] <= cross_parameter1 && cosL[j] >= 0 && (x_left[j] - x_left[j + gap] >= cross_parameter2) && (My_Abs(x_left[j - gap] - x_left[j]) <= 4))
			{
				cross_outL = j;
				//printf("我找点了！！\n");
				break;
			}
		}
		for (uint8_t j = (cross_enterR - gap); j >= (i_end + 2); j--)
		{
			if (cosR[j] <= cross_parameter1 && cosR[j] >= 0 && (x_right[j + gap] - x_right[j] >= cross_parameter2) && (My_Abs(x_right[j] - x_right[j - gap]) <= 4))
			{
				cross_outR = j;
				break;
			}
		}
		if (cross_outL != MISS && cross_outR != MISS)
			cross_flag = 0;
	}
	else if (l_enter != MISS && r_enter != MISS)
	{
		for (uint8_t j = (i_start - gap - 1); j >= (i_end + 2); j--)
		{
			if (cosL[j] <= cross_parameter1 && cosL[j] >= 0 && (x_left[j] - x_left[j + gap] >= cross_parameter2) && (My_Abs(x_left[j - gap] - x_left[j]) <= 3))
			{
				cross_outL = j;
				break;
			}
		}
		for (uint8_t j = (i_start - gap - 1); j >= (i_end + 2); j--)
		{
			if (cosR[j] <= cross_parameter1 && cosR[j] >= 0 && (x_right[j + gap] - x_right[j] >= cross_parameter2) && (My_Abs(x_right[j] - x_right[j - gap]) <= 3))
			{
				cross_outR = j;
				break;
			}
		}
		if (cross_outL != MISS && cross_outR != MISS)
			cross_flag = 1;
	}
	printf("左出口在：%d，右出口在%d\n", cross_outL, cross_outR);
	if (cross_flag == 0)
		return 1;
	else if (cross_flag == 1)
		return 2;
	else return 0;
	/*printf("左出口在：%d，右出口在：%d\n", cross_outL, cross_outR);
	a = (cross_outL + cross_outR) / 2;
	for (int i = 0; i <= 187; i++)
	{
		IMG[a][i] = 4;
	}*/
}

////////////////////////////////////////////
//功能：十字决定双边
//输入：cross_enterL,cross_enterR,cross_outL,cross_outR,x_left,x_right
//输出：left_line,right_line
//备注：
//日期：2021.3.10
///////////////////////////////////////////
void cross_oridinary_two_line(void)
{
	if (my_cross == 1)
	{
		//确定十字存在，开始补线
		//printf("补线！\n");
		parameterB = (x_right[cross_enterR] - static_cast<float>(x_right[cross_outR])) / (cross_enterR - cross_outR);
		parameterA = x_right[cross_enterR] - parameterB * cross_enterR;
		for (uint8_t b = cross_outR; b < (cross_enterR); b++)
		{
			right_line[b] = parameterA + parameterB * b;
			//printf("right_line:%d！\n",right_line[b]);
		}

		parameterB = (x_left[cross_outL] - static_cast<float>(x_left[cross_enterL])) / (cross_outL - cross_enterL);
		parameterA = x_left[cross_enterL] - parameterB * cross_enterL;
		for (uint8_t b = cross_outL; b < (cross_enterL); b++)
		{
			int left = parameterA + parameterB * b;
			if (left >= 0)
				left_line[b] = (uint8_t)left;
			else
				left_line[b] = 0;
		}
	}
	if (my_cross == 2)
	{
		parameterB = (x_right[cross_outR] - static_cast<float>(x_right[cross_outR - 10])) / (float)10;
		parameterA = x_right[cross_outR] - parameterB * cross_outR;
		for (uint8_t b = cross_outR; b <= NEAR_LINE; b++)
		{
			right_line[b] = parameterA + parameterB * b;
		}
		parameterB = (x_left[cross_outL] - static_cast<float>(x_left[cross_outL - 10])) / (float)10;
		parameterA = x_left[cross_outL] - parameterB * cross_outL;
		for (uint8_t b = cross_outL; b <= NEAR_LINE; b++)
		{
			int left = parameterA + parameterB * (int)b;
			if (left >= 0)
				left_line[b] = (uint8_t)left;
			else
				left_line[b] = 0;
		}
	}
}

////////////////////////////////////////////
//功能：识别是否出赛道
//输入：
//输出：赋值给img_protect，输出1时表示出赛道
//备注：2021.1.15，新算法减算力
///////////////////////////////////////////
bool check_out(void)
{
	uint8_t black_flag = 0;
	for (uint8_t i = 70; i >= 30; i--)
	{
		//IMG[i][94] = red;
		if (IMG[i][70] == black) black_flag++;
	}

	if (black_flag >= 35) return 1;
	else return 0;
}

////////////////////////////////////////////
//功能：识别是否存在斑马线
//输入：
//输出：赋值给zebra_change，输出时表示识别到斑马线
//备注：2021.1.28
///////////////////////////////////////////
uint8_t check_zebra(void)
{
	uint8_t count = 0;
	uint8_t start = 0;

	uint8_t m_start = 80;
	uint8_t m_end = 15;
	uint8_t n_start = 30;
	uint8_t n_end = 115;

	uint8_t num_max = MISS;

	uint8_t parameter = 35;//斑马线处第一条白条子的长度阈值
	//最上端要是有无赛道部分，则不可能是斑马线
	/*for (uint8_t i = 20; i > 1; i--)
	{
		if (my_road[i].white_num == 0)
		{
			return MISS;
		}
	}*/

	for (uint8_t m = m_start; m >= m_end; m--)
	{
		//change = 0;
		//test = 0;
		//flag = 0;
		//for (int n = n_start; n <= n_end; n++)
		//{
		//	if (IMG[m][n] == 0)
		//		test = 0;
		//	else
		//		test = 1;
		//	if (test != flag)
		//	{
		//		change++;//发生出现突变时，change加一，同时保存变化，以便下次出现变化可识别
		//		flag = test;
		//	}
		//}
		//if (change >= 12)//共八条斑马线，进出各一次，共十六次，加上边界18次，允许部分误差
		//{
		//	count++;
		//}
		//if (count == 1)
		//{
		//	start = m;
		//}
		//2021.3.16号更改

		if (my_road[m].white_num >= 7)
		{
			count++;
			zebra_end_line = m;
		}
		if (count == 1)
		{
			start = m;
		}
	}
	if (count >= 2)
	{
		//printf("第1个白条长度:%d\n", my_road[start].connected[1].width);
		//printf("最后个白条长度:%d\n", my_road[start].connected[my_road[start].white_num].width);
		if (my_road[start].connected[1].width >= parameter)
		{
			garage_dir = 0;
			//printf("车库在左侧\n");
		}
		else if (my_road[start].connected[my_road[start].white_num].width >= 10)
		{
			garage_dir = 1;
			//printf("车库在右侧\n");
		}
		//printf("count:%d\n", count);
		return start;
	}
	else
	{
		//printf("count:%d\n", count);
		return MISS;
	}
}

////////////////////////////////////////////
//功能：斑马线补线
//输入：
//输出：
//备注：
///////////////////////////////////////////
void clean_zebra(void)
{
	uint8_t m_start = NEAR_LINE - gap;
	uint8_t m_end=FAR_LINE;
	uint8_t zebra_enter = MISS;
	uint8_t zebra_out = MISS;

	uint8_t zebra_parameter1 = 25;
	uint8_t zebra_parameter2 = 35;
	if (garage_dir == 0)
	{
		for (uint8_t m = m_start; m >= zebra_start_line; m--)
		{
			//printf("第%d行第1个白条长度:%d\n", m, my_road[m].connected[1].width);
			if (my_road[m].connected[1].width > my_road[m + gap].connected[1].width)
			{
				if (my_road[m].connected[1].width - my_road[m + gap].connected[1].width > zebra_parameter1)
					zebra_enter = m + gap;
			}
			if (zebra_enter != MISS)
			{
				printf("zebra_enter:%d,", zebra_enter);
				break;
			}
		}
		for (uint8_t m = (zebra_end_line - 1); m >= FAR_LINE; m--)
		{
			if (my_road[m].connected[1].width - my_road[m - gap].connected[1].width > zebra_parameter2)
				zebra_out = m - gap;
			if (zebra_out != MISS)
			{
				printf("zebra_out:%d\n", zebra_out);
				break;
			}
		}
	}
	if (zebra_enter != MISS && zebra_out != MISS)
	{
		for (uint8_t m = zebra_enter; m >= zebra_out; m--)
		{
			right_line[m] = my_road[m].connected[my_road[m].white_num].right;
			//printf("x_right[%d]:%d\n", m, x_right[m]);
		}

		parameterB = (x_left[zebra_out] - static_cast<float>(x_left[zebra_enter])) / (zebra_out - zebra_enter);
		parameterA = x_left[zebra_enter] - parameterB * (zebra_enter);
		for (uint8_t b = (zebra_out); b < (zebra_enter); b++)
		{
			int left = parameterA + parameterB * b;
			if (left >= 0)
				left_line[b] = (uint8_t)left;
			else
				left_line[b] = 0;
		}
	}
	else if (zebra_out != MISS)
	{
		for (uint8_t m = (zebra_start_line + gap); m >= zebra_out; m--)
		{
			right_line[m] = my_road[m].connected[my_road[m].white_num].right;
			//printf("x_right[%d]:%d\n", m, x_right[m]);
		}
		parameterB = (x_left[zebra_out-gap] - static_cast<float>(x_left[zebra_out])) / (-gap);
		parameterA = x_left[zebra_out] - parameterB * (zebra_out);
		for (uint8_t b = (zebra_out); b < m_start; b++)
		{
			int left = parameterA + parameterB * b;
			if (left >= 0)
				left_line[b] = (uint8_t)left;
			else
				left_line[b] = 0;
		}
	}
}

road trident_road[CAMERA_H];
bool left_straight = 0;
bool right_straight = 0;

////////////////////////////////////////////
//功能：判断是否为三岔路
//输入：
//输出：
//备注：
///////////////////////////////////////////
uint8_t trident(void)
{
	uint8_t istart = NEAR_LINE;
	uint8_t iend = 10;
	int while_range_num = 0, roud_while_range_num = 0;
	all_range* twhite_range = NULL;
	road* tmy_road = NULL;
	road* ttrident_road = NULL;
	uint8_t* left_ptr = NULL;
	uint8_t* right_ptr = NULL;
	uint8_t* left_side_ptr = NULL;
	uint8_t* right_side_ptr = NULL;
	uint8_t width_max = 0;
	road_range temp = { 0 };
	uint8_t count = 0;
	uint8_t two_side_account = 0, left_side_account = 0, right_side_account = 0;
	float k_left = 0, k_right = 0;
	printf("三岔判断进行\n");
	for (int i = istart; i >= iend; i--)
	{
		tmy_road = &my_road[i];
		ttrident_road = &trident_road[i];
		while_range_num = tmy_road->white_num;
		roud_while_range_num = 0;
		left_ptr = &left_line[i];
		right_ptr = &right_line[i];
		left_side_ptr = &left_side[i];
		right_side_ptr = &right_side[i];
		/*找出所有符合条件的白条*/
		for (int j = 1; j <= while_range_num; j++)
		{
			if (tmy_road->connected[j].width >= 15 && (tmy_road->connected[j].right <= (CAMERA_W / 2 + 3) || (tmy_road->connected[j].left >= (CAMERA_W / 2 - 3))))
			{
				roud_while_range_num++;
				ttrident_road->white_num++;
				ttrident_road->connected[roud_while_range_num].left = tmy_road->connected[j].left;
				ttrident_road->connected[roud_while_range_num].right = tmy_road->connected[j].right;
				ttrident_road->connected[roud_while_range_num].width = tmy_road->connected[j].width;
			}
		}
		if (0 == roud_while_range_num || 1 == roud_while_range_num)	//左右白条数为0或者1，不是三岔路
		{
			trident_road[i] = { 0 };
		}
		/*如果找出超过两个白条，选择最大的两个白条作为三岔路的两分支*/
		/*插入法排序，使前两个白条宽度为最大*/
		else if (ttrident_road->white_num >= 3)
		{
			for (int m = 2; m <= roud_while_range_num; m++)//插入法也许会写错，可能出BUG
			{
				temp = ttrident_road->connected[m];
				for (int n = m - 1; (n >= 1) && (ttrident_road->connected[n].width < temp.width); n--)
				{
					ttrident_road->connected[m] = ttrident_road->connected[n];
					ttrident_road->connected[n] = temp;
				}
			}
			ttrident_road->white_num = 2;
			if (ttrident_road->connected[1].left > ttrident_road->connected[2].left)
			{
				temp = ttrident_road->connected[1];
				ttrident_road->connected[1] = ttrident_road->connected[2];
				ttrident_road->connected[2] = temp;
			}
		}
		if (ttrident_road->white_num == 2)
			count++;

		if (abs(*left_ptr - *left_side_ptr) <= 8 && abs(*right_side_ptr - *right_ptr) <= 8)
		{
			two_side_account++;
		}
		else if (abs(*left_ptr - *left_side_ptr) <= 8 && abs(*right_side_ptr - *right_ptr) > 8)
		{
			left_side_account++;
		}
		else if (abs(*left_ptr - *left_side_ptr) > 8 && abs(*right_side_ptr - *right_ptr) <= 8)
		{
			right_side_account++;
		}
	}
	k_left = (float)(left_line[95] - left_line[10]) / 85.0;
	k_right = (float)(right_line[95] - right_line[10]) / 85.0;
	if (count >= 8)
	{
		left_straight = define_straight_line(left_line, 110, 50);
		right_straight = define_straight_line(right_line, 110, 50);
		if ((abs(k_left) <= 0.5) || (abs(k_right) <= 0.5))
		{
			return 0;
		}
		/*left_straight = define_straight_line(left, 110, 50, 1);
		right_straight = define_straight_line(right, 110, 50, 2);
		if ((left_straight && !right_straight) ||  (!left_straight && right_straight))
		{
			printf("左右方差不符合\n");
			return 0;
		}*/

		//printf("l:%d,r:%d\n", left_straight, right_straight);
		//printf("left:%d,right:%d", left_side_account, right_side_account);
		if (left_side_account >= 40 || right_side_account >= 40)
		{
			printf("左右靠边行数过多\n");
			return 0;
		}
		return 1;
	}

	else
	{
		printf("count = %d\n", count);
		return 0;
	}
}

////////////////////////////////////////////
//功能：检查是否为岔道/check if it is fork
//输入：left_line[CAMERA_H],right_line[CAMERA_H]
//输出：
//备注：2021.1.22
///////////////////////////////////////////
void check_fork(void)
{
	uint8_t a;

	uint8_t fork_range = 0;//岔道处上方顶点位置

	uint8_t fork_width = 35;//岔道白块左右宽度

	printf("岔道左断点%d，岔道右断点%d\n", fork_breakL, fork_breakR);
	/*printf("十字左断点%d，十字右断点%d\n", cross_breakL, cross_breakR);*/


	if ((My_Abs(fork_breakL - fork_breakR) <= 20) && (fork_breakL != 0) && (fork_breakR != 0) && my_cross == 0)
	{
		a = (x_left[fork_breakL] + x_right[fork_breakR]) / 2;
		for (int m = 0; m <= CAMERA_H; m++)
		{
			if (IMG[m][a] == 1)
			{
				fork_range = m;
				break;
			}
		}
		if (fork_range >= 3)
		{
			my_fork = 1;
			fork_test = 1;
		}
	}
	else
	{
		my_fork = 0;
		/*if (fork_test == 1)
		{
			fork_flag++;
			fork_test = 0;
		}
		else
			fork_test = 0;*/
	}
	fork_flag = 2;
	if (fork_range != 0)
	{
		for (uint8_t n = 0; n <= 187; n++)
		{
			IMG[fork_range][n] = 6;
		}
	}

	/*if (My_Abs(cross_breakL - cross_breakR) <= 5)
		my_cross = 1;
	if ((cross_breakL == 0) && (cross_breakR == 0))
		my_cross = 0;*/

	if (my_fork == 1)
	{
		if (((fork_flag / 2) % 2) == 1)//奇数次碰到岔路，向左转,右侧补线
		{
			parameterB = (x_right[fork_breakR] - static_cast<float>(a)) / (fork_breakR - fork_range);//从右断点和顶点取直线斜率
			parameterA = x_right[fork_breakR] - parameterB * fork_breakR;//从右断点取右线截距
			float parameterC = x_left[fork_range + 5] - parameterB * (fork_range + 5);//从左边线与顶点处往上取截距
			for (int b = 0; b < fork_range; b++)
			{
				int left = parameterC + parameterB * b;
				if (left >= 0)
					x_left[b] = left;
				else
					x_left[b] = 0;
				x_right[b] = parameterA + parameterB * b;
			}
			for (int c = fork_range; c < fork_breakR; c++)
			{
				x_right[c] = parameterA + parameterB * c;
			}
		}
		if (((fork_flag / 2) % 2) == 0)//偶数次碰到岔路，向右转,左侧补线
		{
			parameterB = ((float)(x_left[fork_breakL] - static_cast<float>(a))) / ((float)(fork_breakL - fork_range));//从左断点和顶点取直线斜率
			parameterA = x_left[fork_breakL] - parameterB * fork_breakL;//从左断点取左线截距
			float parameterD = x_right[fork_range + 5] - parameterB * (fork_range + 5);//从右边线与顶点处往上取截距
			for (int d = 0; d < fork_range; d++)
			{
				x_left[d] = parameterA + parameterB * d;
				x_right[d] = parameterD + parameterB * d;
			}
			for (int e = fork_range; e < fork_breakL; e++)
			{
				x_right[e] = parameterA + parameterB * e;
			}
			check_right_line(0, fork_range);
		}
	}

}

//////////////////////////////////////////////
////功能：大角度弯道修正
////输入：
////输出：输出1时进行了大角度弯道修正，输出零时未进行大角度弯道修正
////备注：直接对x_left和x_right进行修正
/////////////////////////////////////////////
bool roof(void)
{
	uint8_t roofL = 0;
	uint8_t roofR = 0;

	uint8_t roof_line = MISS;
	uint8_t roof_start_line = 95;//寻找left_min和right_min的最近行
	uint8_t roof_end_line = 80;//寻找roof_line的最远行
	uint8_t roof_parameter1 = 30;//left_min和right_min的绝对值差值:30
	uint8_t roof_parameter2 = 20;//往下取值的距离:20
	uint8_t roof_parameter5 = MISS;//实际取值位置:即roof_parameter+roofL/roofR
	uint8_t roof_parameter3 = 10;//盲补线的修正值:10
	uint8_t roof_parameter4 = 10;//寻找突变沿的最小参数
	//寻找第一个非miss行
	for (uint8_t i = 2; i <= roof_start_line; i++)
	{
		if (x_left[i] != MISS || x_right[i] != MISS)
		{
			roof_line = i;
			break;
		}
	}
	printf("roof_line: %d\n", roof_line);
	if (roof_line == MISS)
		return 0;
	uint8_t left_min = MISS;
	uint8_t right_min = MISS;

	uint8_t vet = 0;
	int left = 0;
	uint8_t right = 0;
	for (uint8_t i = roof_start_line; i >= roof_line; i--)
	{
		if ((x_left[i] - left_side[i]) < left_min)
		{
			left_min = (x_left[i] - left_side[i]);
			//roofL = i;
		}
		if (x_left[i] <= (left_side[i] + 4))
		{
			left_min = (x_left[i] - left_side[i]);
			//roofL = i;
		}
		if ((My_Abs(x_left[i] - x_left[i + 1]) >= roof_parameter4) && (roofL == 0))
		{
			roofL = i;
		}
		//printf("行数：%d，left_min：%d\n", i, left_min);
	}
	for (uint8_t i = roof_start_line; i >= roof_line; i--)
	{
		if ((right_side[i] - x_right[i]) < right_min)
		{
			right_min = right_side[i] - x_right[i];
			//roofR = i;
		}
		if ((right_side[i] - x_right[i]) <= 3)
		{
			right_min = right_side[i] - x_right[i];
			//roofR = i;
		}
		if ((My_Abs(x_right[i] - x_right[i + 1]) >= roof_parameter4) && (roofR == 0))
		{
			roofR = i;
		}
		//printf("行数：%d，right_min：%d\n", i, right_min);
	}
	//printf("圆弧顶修正: 左%d %d  右%d %d\n", left_min, roofL, right_min, roofR);
	if (left_min < right_min)
	{
		if (right_min - left_min > roof_parameter1)
		{
			printf("大角度弯道左修正：roofL: %d , roofR: %d\n", roofL, roofR);
			roof_parameter5 = roofL + roof_parameter2;
			if (roof_parameter5 >= NEAR_LINE)
			{
				roof_parameter5 = NEAR_LINE - 1;
			}
			vet = x_right[roof_parameter5] - x_left[roof_parameter5];
			//printf("盲补修正值：%d，L:%d，R：%d", vet, x_left[roofL + roof_parameter2], x_right[roofL + roof_parameter2]);
			for (uint8_t j = roofL; j > roof_line; j--)
			{
				left = (int)x_right[j] - vet - roof_parameter3;
				if (left >= 0)
					x_left[j] = left;
				else
					x_left[j] = left_side[j];
			}
			return 1;
		}
	}
	else if (left_min > right_min)
	{
		if (left_min - right_min > roof_parameter1)
		{
			printf("大角度弯道右修正：roofL: %d , roofR: %d\n", roofL, roofR);
			roof_parameter5 = roofR + roof_parameter2;
			if (roof_parameter5 >= NEAR_LINE)
			{
				roof_parameter5 = NEAR_LINE - 1;
			}
			vet = x_right[roof_parameter5] - x_left[roof_parameter5];
			//printf("盲补修正值：%d，L:%d，R：%d", vet, x_left[roofR + roof_parameter2], x_right[roofR + roof_parameter2]);
			for (int j = roofR; j > roof_line; j--)
			{
				right = x_left[j] + vet + roof_parameter3;
				if (right < CAMERA_W)
					x_right[j] = right;
				else
					x_right[j] = right_side[j] - 1;
			}
			return 1;
		}
	}
	return 0;
}

////////////////////////////////////////////
//功能：识别是否为直道
//输入：
//输出：直道标志量：1为直道，2为左线不直，3为右线不直，4为左右均不直
//备注：
//日期：2021.1.29
///////////////////////////////////////////
uint8_t check_straight(void)
{
	//printf("x_left_straight:%d,x_right_straight:%d\n", left_flag, right_flag);
	if ((x_left_straight == 1) && (x_right_straight == 1))
	{
		mood.chuhuandao = 0;
		mood.ruhuandao = 0;
		mood.circle = 0;
		return 1;
	}
	else if ((x_left_straight == 0) && (x_right_straight == 1))
		return 2;
	else if ((x_left_straight == 1) && (x_right_straight == 0))
		return 3;
	else
		return 4;
}

////////////////////////////////////////////
//功能：数组初始化
//输入：uint8_t* ptr 数组首地址, uint8_t num初始化的值, uint8_t size数组大小
//输出：
//备注：因为k66库中认为memset函数不安全，所以无法使用；因此需要自己写一个my_memset
///////////////////////////////////////////
void my_memset(uint8_t* ptr, uint8_t num, uint8_t size)
{
	uint8_t* p = ptr;
	uint8_t my_num = num;
	uint8_t Size = size;
	for (uint8_t i = 0; i < Size; i++, p++)
	{
		*p = my_num;
	}
}

////////////////////////////////////////////
//功能：检查right_line中的异常情况，并将它限幅
//输入：start：起始行；end:终止行
//输出：限幅后的right_line
//备注：2021.1.23
///////////////////////////////////////////
void check_right_line(uint8_t start, uint8_t end)
{
	for (uint8_t i = start; i <= end; i++)
	{
		if (right_line[i] > CAMERA_W)
			right_line[i] = CAMERA_W - 1;
	}
}

////////////////////////////////////////////
//功能：初始化元素
//输入：
//输出：
//备注：
///////////////////////////////////////////
void element_init(void)
{
	//memset(mid_line, MISS, CAMERA_H);
	memset(left_line, MISS, CAMERA_H);
	memset(right_line, MISS, CAMERA_H);
	memset(left_break_down, MISS, 2);
	memset(right_break_up, MISS, 2);
	memset(break_point_on_right, MISS, 6);
	memset(break_point_on_left, MISS, 6);
	memset(left_smooth, MISS, CAMERA_H);
	memset(right_smooth, MISS, CAMERA_H);
	memset(x_left, MISS, CAMERA_H);
	memset(x_right, MISS, CAMERA_H);


	how_many_break_on_right = 0;
	how_many_break_on_left = 0;

	garage_dir = MISS;

	my_cross = 0;

}

////////////////////////////////////////////
//功能：中线合成
//输入：左右边界
//输出：中线
//备注：
///////////////////////////////////////////
void get_mid_line(void)
{
	memset(mid_line, MISS, CAMERA_H);
	for (int i = NEAR_LINE; i >= FAR_LINE; i--)
		if (x_left[i] != MISS)
		{
			mid_line[i] = (left_smooth[i] + right_smooth[i]) / 2;
		}
		else
		{
			mid_line[i] = MISS;
		}

}

////////////////////////////////////////////
//功能：前后求平均位置滤波
//输入：uint8_t left_line[CHANGED_H];//左边线  uint8_t right_line[CHANGED_H];//右边线
//输出：uint8_t left_smooth[CHANGED_H];//左滤波 uint8_t right_smooth[CHANGED_H];//右滤波
//备注：copy学长的学长
//日期：2021.2.6
///////////////////////////////////////////
void filter_two_line(void)
{
	uint8_t i_start;
	uint8_t i_end;
	uint8_t i;
	int j_add = 0;
	uint8_t num = 2;//向上或向下要取多少个点
	uint8_t sum;
	uint8_t left_top = NEAR_LINE - gap, right_top = NEAR_LINE + gap;
	sum = 2 * num + 1;//每个点的值由周围总共几个点决定

	for (i = NEAR_LINE; i >= FAR_LINE; i--)
		if (left_line[i] != MISS)left_top = i;
		else break;

	for (i = NEAR_LINE; i >= FAR_LINE; i--)
		if (right_line[i] != MISS)right_top = i;
		else break;



	memset(left_smooth, MISS, CHANGED_H);
	memset(right_smooth, MISS, CHANGED_H);

	//左边界

	i_start = NEAR_LINE - num;
	i_end = left_top + num;

	for (i = NEAR_LINE; i >= i_start + 1; i--)
		left_smooth[i] = left_line[i];

	for (i = i_end - 1; i >= left_top; i--)
		left_smooth[i] = left_line[i];

	j_add = 0;
	for (i = NEAR_LINE; i >= NEAR_LINE - sum + 1; i--)
		j_add += left_line[i];

	for (i = i_start; i >= i_end; i--)
	{
		left_smooth[i] = j_add / sum;
		//printf("我滤波了一下!!!\n");
		//IMG[i][left_smooth[i]] = green;
		if (i == i_end)break;
		j_add -= left_line[i + num];
		j_add += left_line[i - num - 1];
	}

	//右边界
	i_start = NEAR_LINE - num;
	i_end = right_top + num;

	for (i = NEAR_LINE; i >= i_start + 1; i--)
		right_smooth[i] = right_line[i];

	for (i = i_end - 1; i >= right_top; i--)
		right_smooth[i] = right_line[i];

	j_add = 0;
	for (i = NEAR_LINE; i >= NEAR_LINE - sum + 1; i--)
		j_add += right_line[i];

	for (i = i_start; i >= i_end; i--)
	{
		right_smooth[i] = j_add / sum;
		//printf("right_sommoth[%d]:%d\n", i, right_smooth[i]);
		//IMG[i][right_smooth[i]] = green;
		if (i == i_end)break;
		j_add -= right_line[i + num];
		j_add += right_line[i - num - 1];
	}

}//filter_two_line

////////////////////////////////////////////
//功能：找中线(滤波之后)
//输入：uint8_t left_smooth[CHANGED_H];//左滤波 uint8_t right_smooth[CHANGED_H];//右滤波
//输出：mid_float
//备注：赛道两边的对应点距离最短
///////////////////////////////////////////
void find_mid(void)
{
	uint8_t i_left;
	uint8_t i_right;
	int k;
	int dis_l2;//如果延长左侧的距离平方
	int dis_r2;//如果延长右侧的距离平方
	int k_x = 1;//量纲系数
	int k_y = 1;//量纲系数？？？4.23  5改1
	int x;
	int y;
	uint8_t i_fore = 3;
	uint8_t break_flag = 0;//断线的方向,0为左，1为右
	uint8_t break_num = 0;//断线的数量，最多为2
	uint8_t num;//与多少个之前的点延长
	i_left = NEAR_LINE - gap;///////////
	i_right = NEAR_LINE - gap;///////////

	//for (int i = NEAR_LINE; i >= FAR_LINE; i--)
	//	printf("%d %d,  %d %d\n", i, left_smooth[i], i, right_smooth[i]);

	//for (int i = NEAR_LINE; i >= FAR_LINE; i--)
	//	if (left_smooth[i] != MISS)IMG[i][left_smooth[i]] = red;

	//for (int i = NEAR_LINE; i >= FAR_LINE; i--)
	//	if (right_smooth[i] != MISS)IMG[i][right_smooth[i]] = red;

	for (k = 1; k < MID_MAX; k++)
	{
		mid_float[k].i = MISS;
		mid_float[k].j = MISS;


		if (break_num == 0)
		{
			//	printf("%d * (%d %d) (%d %d)\n",i_fore,i_left,left_smooth[i_left],i_right,right_smooth[i_right]);
			x = (int)left_smooth[i_left - i_fore] - (int)right_smooth[i_right];
			y = (int)i_left - (int)i_fore - (int)i_right;
			dis_l2 = k_x * x * x + k_y * y * y;
			x = (int)left_smooth[i_left] - (int)right_smooth[i_right - i_fore];
			y = (int)i_left - (int)i_right + (int)i_fore;
			dis_r2 = k_x * x * x + k_y * y * y;
			//printf("k:%d  dis_l2:%d  dis_r2：%d\n",k,dis_l2,dis_r2);
			if (dis_l2 == dis_r2)
			{
				i_left--;
				i_right--;
			}
			else if (dis_l2 > dis_r2)
			{
				i_right--;
			}
			else
			{
				i_left--;
			}
			if (i_left < FAR_LINE || i_right < FAR_LINE) break;
			mid_float[k].i = ((float)i_left + (float)i_right) / 2;
			mid_float[k].j = ((float)left_smooth[i_left] + (float)right_smooth[i_right]) / 2;

			if (
				(
					left_smooth[i_left - i_fore] == MISS
					||
					right_smooth[i_right - i_fore] == MISS
					)
				&&
				i_fore > 1
				)
			{
				//break;
				i_fore--;
			}
			if (
				i_left < i_fore
				||
				left_smooth[i_left - 1] == MISS
				)
			{
				break_num++;
				break_flag = 0;
			}
			if (
				i_right < i_fore
				||
				right_smooth[i_right - 1] == MISS
				)
			{
				break_num++;
				break_flag = 1;
			}
		}
		else if (break_num == 1)
		{
			//当一边丢失的时候，仅仅根据一边走
			if (break_flag)
			{
				//沿左侧，右侧断
				//printf("by left\n");
				//沿左侧
				i_left--;
				mid_float[k].i = mid_float[k - 1].i - 1;
				mid_float[k].j = mid_float[k - 1].j + (float)left_smooth[i_left] - (float)left_smooth[i_left + 1];
				if (
					mid_float[k].i < FAR_LINE
					||
					mid_float[k].j< 0
					||
					mid_float[k].j> CHANGED_W - 1
					)
				{
					break_num++;
					k--;
					continue;
				}
				//断线判断
				if (
					i_left < FAR_LINE
					||
					left_smooth[i_left - 1] == MISS
					)
				{
					break_num++;
				}
			}
			else
			{
				//沿右侧，左侧断
				//printf("by right\n");
				//沿左侧
				i_right--;
				mid_float[k].i = mid_float[k - 1].i - 1;
				mid_float[k].j = mid_float[k - 1].j + (float)right_smooth[i_right] - (float)right_smooth[i_right + 1];
				if (
					mid_float[k].i < FAR_LINE
					||
					mid_float[k].j< 0
					||
					mid_float[k].j> CHANGED_W - 1
					)
				{
					break_num++;
					k--;
					continue;
				}
				//断线判断
				if (
					i_right < FAR_LINE
					||
					right_smooth[i_right - 1] == MISS
					)
				{
					break_num++;
				}
			}
		}
		else
		{
			//寻线结束
			break;
		}


	}


	//for (int i = 1; i < 94; i++)
	//	IMG[i_left][i] = red;

	//for (int i = 95; i < 188; i++)
	//	IMG[i_right][i] = green;

	for (int i = 0; i <= k; i++)
		IMG[(int)mid_float[i].i][(int)mid_float[i].j] = green;

			//断线延长

	uint8_t extern_num = 20;//与多少个之前的点延长  2020.7.26改20
	uint8_t i_start, i_end;//延长起点与中点
	uint8_t tk;
	uint8_t bxref_actualnum = 0;
	int tnum = 0;
	float a = 0, b = 0, x_average = 0, y_average = 0, xy_average = 0, x2_average = 0;

	num = k;
	if (num > extern_num)
	{
		num = extern_num;
	}
	if (k == 0 || k == 1)return;
	//最小二乘法延长中线
	//y 列 x 行
	a = 0;
	b = 0;
	x_average = 0;
	y_average = 0;
	xy_average = 0;
	x2_average = 0;
	bxref_actualnum = 0;
	i_start = k - num;
	if (i_start <= 1) i_start = 1;
	i_end = k - 1;
	//printf("%d %d %f %f", i_start, i_end, mid_float[i_start].i, mid_float[i_end].i);
	//for (int i = 0; i < 119; i++)
	//{
	//	IMG[(int)mid_float[i_start].i][i] = red;
	//	IMG[(int)mid_float[i_end].i][i] = red;
	//}

	for (tk = i_start; tk <= i_end; tk++)
	{
		x_average += mid_float[tk].i;
		y_average += mid_float[tk].j;
		xy_average += mid_float[tk].j * mid_float[tk].i;
		x2_average += mid_float[tk].i * mid_float[tk].i;
		bxref_actualnum++;
	}
	x_average /= bxref_actualnum;
	y_average /= bxref_actualnum;
	xy_average /= bxref_actualnum;
	x2_average /= bxref_actualnum;
	if ((x_average * x_average - x2_average) != 0)
		a = (x_average * y_average - xy_average) / (x_average * x_average - x2_average);
	else a = 0;
	b = y_average - a * x_average;

	for (tk = k; tk < MID_MAX; tk++)
	{
		if (mid_float[tk - 1].i <= FAR_LINE)
		{
			mid_float[tk].i = MISS;
			mid_float[tk].j = MISS;
			break;
		}
		tnum = (int)(a * mid_float[tk - 1].i + b);
		if (tnum > 0 && tnum < CAMERA_W)
		{
			mid_float[tk].i = mid_float[tk - 1].i - 1;
			mid_float[tk].j = tnum;
			IMG[(int)mid_float[tk].i][(int)mid_float[tk].j] = red;
		}
		else
		{
			mid_float[tk].i = MISS;
			mid_float[tk].j = MISS;
			break;
		}
	}

	//for (int i = 0; i <= 200; i++)
	//{
	//	printf("i:%f  j:%f\n", mid_float[i].i, mid_float[i].j);
	//	IMG[(int)mid_float[i].i][(int)mid_float[i].j] = green;
	//}
	//
	//for (int i = 0; i < 119; i++)
	//{
	//	IMG[(int)mid_float[i_start].i][i] = red;
	//	IMG[(int)mid_float[i_end].i][i] = red;
	//}
	////最小二乘法延长右边界y = ax + b;
	////y 列 x 行
	//a = 0;
	//b = 0;
	//x_average = 0;
	//y_average = 0;
	//xy_average = 0;
	//x2_average = 0;
	//bxref_actualnum = 0;
	//i_start = NEAR_LINE;
	//if (stable_right != MISS)i_end = stable_right;
	//else i_end = NEAR_LINE - bxref_max;
	//x_average = (float)(i_start + i_end) / 2;
	//for (i = i_start; i >= i_end; i--)
	//	if (x_right[i] != MISS)
	//	{
	//		y_average += x_right[i];
	//		xy_average += x_right[i] * i;
	//		x2_average += i * i;
	//		bxref_actualnum++;
	//	}
	//y_average /= (bxref_actualnum);
	//xy_average /= (bxref_actualnum);
	//x2_average /= (bxref_actualnum);
	//a = (x_average*y_average - xy_average) / (x_average*x_average - x2_average);
	//b = y_average - a * x_average;

	//for (i = bxbegin; i >= bxend; i--)
	//{
	//	tnum = (int)(a * i + b);
	//	if (tnum > 0 && tnum < CAMERA_W)
	//		rstr[i] = tnum;
	//	else rstr[i] = MISS;
	//}


	//num = k;
	//if (!num)
	//{
	//	num = 1;
	//	k = 1;
	//}
	//if (num > extern_num)
	//{
	//	num = extern_num;
	//}

	//if (mid_float[k - num].i - mid_float[k - 1].i != 0)
	//	mid_add = (mid_float[k - 1].j - mid_float[k - num].j) / (mid_float[k - num].i - mid_float[k - 1].i);
	//else mid_add = 0;
	//for (; k < MID_MAX; k++)
	//{
	//	mid_float[k].i = mid_float[k - 1].i - 1;
	//	mid_float[k].j = mid_float[k - 1].j + mid_add;
	//	//出界判断
	//	if (
	//		(mid_float[k].j<0 || mid_float[k].j>CHANGED_W - 1)
	//		||
	//		(mid_float[k].i<FAR_LINE || mid_float[k].i>NEAR_LINE)
	//		)
	//	{
	//		mid_float[k].i = MISS;
	//		mid_float[k].j = MISS;
	//		break;
	//	}
	//}
}//find_mid

void if_connect(void)
{
	for (uint8_t i = 113; i >= 2; i--)
	{
		if (x_left[i] == MISS || x_right[i] == MISS) break;
		if (x_left[i] >= x_right[i - 1] || x_right[i] <= x_left[i - 1])
		{
			for (uint8_t j = i - 1; j >= 1; j--)
			{
				left_smooth[j] = MISS;
				right_smooth[j] = MISS;
			}
			break;
		}
	}
}

void if_smooth(void)
{
	uint8_t g = 10;
	uint8_t i_start, i_end;
	uint8_t Lmiss_flag = MISS, Rmiss_flag = MISS;
	int cosL[CHANGED_H] = { 0 };//余弦的平方左 扩大1000倍
	int cosR[CHANGED_H] = { 0 };//余弦的平方右 扩大1000倍
	int x1 = 0;
	int x2 = 0;
	int y2 = 0;

	i_start = 80; //防止镜头畸变影响
	i_end = 15;
	for (uint8_t i = 113; i >= 1; i--)
	{
		if (my_road_num[i] == 0)
		{
			i_end = i + gap + 1;
			break;
		}
	}


	y2 = g * g;
	for (uint8_t i = i_start; i >= i_end; i--)
	{
		//计算左侧余弦值平方
		x1 = (int)left_smooth[i - g] - (int)left_smooth[i];
		x2 = (int)left_smooth[i] - (int)left_smooth[i + g];
		cosL[i] = x1 * x2 + y2;
		cosL[i] *= cosL[i];
		cosL[i] = cosL[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));


		//计算右侧余弦值平方
		x1 = (int)right_smooth[i - g] - (int)right_smooth[i];
		x2 = (int)right_smooth[i] - (int)right_smooth[i + g];
		cosR[i] = x1 * x2 + y2;
		cosR[i] *= cosR[i];
		cosR[i] = cosR[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));

		//printf("行数：%d 左余弦：%d 右余弦：%d\n", i, cosL[i], cosR[i]);

	}
	for (uint8_t i = i_start; i >= i_end; i--)
	{
		if (cosR[i] <= 700)// || cosL[i] <= 400
		{
			Rmiss_flag = i;
			break;
		}
	}
	for (uint8_t i = i_start; i >= i_end; i--)
	{
		if (cosL[i] <= 700)
		{
			Lmiss_flag = i;
			break;
		}
	}



	if (Lmiss_flag != MISS)
	{
		for (uint8_t i = Lmiss_flag; i >= 1; i--)
		{
			left_smooth[i] = MISS;
		}
	}
	if (Rmiss_flag != MISS)
	{
		for (uint8_t i = Rmiss_flag; i >= 1; i--)
		{
			right_smooth[i] = MISS;
		}
	}
	//uint8_t i_start, i_end;
	//uint8_t Lmiss_flag = MISS, Rmiss_flag = MISS;
	//int cosL[CHANGED_H] = { 0 };//余弦的平方左 扩大1000倍
	//int cosR[CHANGED_H] = { 0 };//余弦的平方右 扩大1000倍
	//int x1 = 0;
	//int x2 = 0;
	//int y2 = 0;

	//i_start = 85 - gap; //防止镜头畸变影响
	//i_end = FAR_LINE + 2 * gap;
	//for (uint8_t i = 113; i >= 1; i--)
	//{
	//	if (my_road_num[i] == 0)
	//	{
	//		i_end = i + gap + 1;
	//		break;
	//	}
	//}


	//y2 = gap * gap;
	//for (uint8_t i = i_start; i >= i_end; i--)
	//{
	//	//计算左侧余弦值平方
	//	x1 = (int)left_smooth[i - gap] - (int)left_smooth[i];
	//	x2 = (int)left_smooth[i] - (int)left_smooth[i + gap];
	//	cosL[i] = x1 * x2 + y2;
	//	cosL[i] *= cosL[i];
	//	cosL[i] = cosL[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));


	//	//计算右侧余弦值平方
	//	x1 = (int)right_smooth[i - gap] - (int)right_smooth[i];
	//	x2 = (int)right_smooth[i] - (int)right_smooth[i + gap];
	//	cosR[i] = x1 * x2 + y2;
	//	cosR[i] *= cosR[i];
	//	cosR[i] = cosR[i] * 1000 / ((x1 * x1 + y2) * (x2 * x2 + y2));

	//	printf("行数：%d 左余弦：%d 右余弦：%d\n", i, cosL[i], cosR[i]);

	//}
	//for (uint8_t i = i_start; i >= i_end; i--)
	//{
	//	if (cosR[i] <= 550)// || cosL[i] <= 400
	//	{
	//		Rmiss_flag = i;
	//		break;
	//	}
	//}
	//for (uint8_t i = i_start; i >= i_end; i--)
	//{
	//	if (cosL[i] <= 550)
	//	{
	//		Lmiss_flag = i;
	//		break;
	//	}
	//}



	//if (Lmiss_flag != MISS)
	//{
	//	for (uint8_t i = Lmiss_flag; i >= 1; i--)
	//	{
	//		left_smooth[i] = MISS;
	//	}
	//}
	//if (Rmiss_flag != MISS)
	//{
	//	for (uint8_t i = Rmiss_flag; i >= 1; i--)
	//	{
	//		right_smooth[i] = MISS;
	//	}
	//}
}

//////////////////////////////////////////////
////功能：2021.1.15 弧形前瞻
////输入：无
////输出：偏差
////备注：（93,119）为圆心 
/////////////////////////////////////////////
float define_PROSPECT(uint8_t PROSPECT)
{
	uint8_t i_start = NEAR_LINE, i_end = FAR_LINE;
	int mid, R2;
	int detle_x, detle_y;
	float tanangle, arctanangle;

	for (uint8_t i = i_start; i >= i_end; i--)
	{
		mid = mid_line[i];
		R2 = (mid - 93) * (mid - 93) + (i - 119) * (i - 119);

		if (R2 >= PROSPECT * PROSPECT)
		{
			detle_x = 93 - mid;
			detle_y = 119 - i;
			tanangle = (float)detle_x / (float)detle_y;
			arctanangle = tanangle - (tanangle * tanangle * tanangle) / 3;  //arctan（x） 麦克劳林展开
			return arctanangle * 100;
			break;
		}
	}
	return 0;
}

////////////////////////////////////////////
//功能：图像程序测试功能
//输入：标志量：img_protect\zebra_change\
//输出：控制台文字说明显示
//备注：仅用于图像组测试程序时所用，写入单片机时可引去该函数
///////////////////////////////////////////
void img_test(void)
{
	/////////////////////////////////////////////////////////////////
	printf("x_left_straight:%d,x_right_straight:%d\n", x_left_straight, x_right_straight);
	/////////////////////////////////////////////////////////////////
	if (inf.if_out == 1)
		printf("出赛道已识别!\n");
	/*else
		printf("出赛道未识别\n");*/
	/////////////////////////////////////////////////////////////////
	if (zebra_start_line != MISS)
	{
		for (uint8_t m = 20; m < (CAMERA_W - 20); m++)
		{
			IMG[zebra_start_line][m] = purple;
			IMG[zebra_end_line][m] = purple;
		}
		if (garage_dir == 0)
		{
			printf("车库在左边\n");
		}
		else
		{
			printf("车库在右边\n");
		}
		printf("斑马线已识别\n");
	}
	/*else
		printf("斑马线未识别\n");*/
	/////////////////////////////////////////////////////////////////
	printf("中线偏差：%f\n", inf.my_angle);
	/////////////////////////////////////////////////////////////////
	/*if (my_garage > 40)
		printf("车库中\n");
	else
		printf("已出库\n");*/
	/////////////////////////////////////////////////////////////////
	if (my_fork == 1)
	{
		printf("第一岔路类型，标志量为%d,", fork_flag);
		if (((fork_flag / 2) % 2) == 0)
			printf("向右补线\n");
		else
			printf("向左补线\n");
	}
	else if (my_fork == 2)
	{
		printf("第二岔路类型，标志量为%d,", fork_flag);
		if (((fork_flag / 2) % 2) == 0)
			printf("向右补线\n");
		else
			printf("向左补线\n");
	}
	else
		printf("岔路未识别\n");
	/////////////////////////////////////////////////////////////////
	//int road_width = 0;//道路宽度
	//int i = 40;//目标行数
	//road_width = right_line[i] - left_line[i];
	//for (int j = 0; j <= 187; j++)
	//	IMG[i][j] = 4;
	//printf("第%d行的道路宽度为%d\n", i, road_width);
	/////////////////////////////////////////////////////////////////
	if (my_straight == 1)
		printf("直道已识别\n");
	else if (my_straight == 2)
		printf("直道未识别：左线不直\n");
	else if (my_straight == 3)
		printf("直道未识别：右线不直\n");
	else if (my_straight == 4)
		printf("直道未识别：左右均线不直\n");
	/////////////////////////////////////////////////////////////////
	if (my_cross == 0)
		printf("十字未识别\n");
	else if (my_cross == 1)
		printf("十字已识别，第一型十字\n");
	else if (my_cross == 2)
		printf("十字已识别，第二型十字\n");
	/////////////////////////////////////////////////////////////////
	printf("图像阈值:%d\n", threshold);
	//////////////////////////////////////////////////////////////////////////////////////赛道涂灰色
	uint8_t* left_side_ptr = NULL;
	uint8_t* right_side_ptr = NULL;
	for (uint8_t i = NEAR_LINE; i >= 1; i--)
	{
		/*if (x_left[i] != MISS && x_right[i] != MISS)
		{
			for (uint8_t j = x_left[i]; j <= x_right[i]; j++)
				IMG[i][j] = sky;
			IMG[i][x_left[i]] = blue;
			IMG[i][x_right[i]] = green;
			IMG[i][mid_line[i]] = red;
		}*/
		/*if (left_line[i] != MISS && right_line[i] != MISS)
		{
			for (uint8_t j = x_left[i]; j <= x_right[i]; j++)
				IMG[i][j] = sky;
			IMG[i][left_line[i]] = blue;
			IMG[i][right_line[i]] = green;
			IMG[i][mid_line[i]] = red;
		}*/
		//printf("my_road[%d].num:%d\n", i, my_road[i].white_num);
		//printf("my_road[%d].connected[1].left:%d\n",i, my_road[i].connected[1].left);
		//printf("x_left[%d]:%d,x_right[%d]:%d\n", i, x_left[i], i, x_right[i]);
		if (x_left[i] != MISS && x_right[i] != MISS)
		{
			for (uint8_t j = x_left[i]; j <= x_right[i]; j++)
				IMG[i][j] = sky;
			IMG[i][left_smooth[i]] = blue;
			IMG[i][right_smooth[i]] = green;
			IMG[i][mid_line[i]] = red;
		}
		/*else
		{
			printf("smooth丢失！！！\n");
		}*/

		/*left_side_ptr = &left_side[i];
		right_side_ptr = &right_side[i];
		for (uint8_t j = 0; j < CAMERA_W; j++)
		{
			if (j <= *left_side_ptr || j >= *right_side_ptr)
				IMG[i][j] = MISS;
		}*/
	}
	/////////////////////////////////////////////////////////////////
	printf("***************************************\n");
	/////////////////////////////////////////////////////////////////
	draw_PROSPECT();
	/*for (int i = 0; i < 187; i++)
	{
		IMG[70][i] = 5;
		IMG[84][i] = 5;
		IMG[90][i] = 5;
	}
	for (int i = 0; i < 119; i++)
	{
		IMG[i][43] = 5;
		IMG[i][135] = 5;
	}*/
}

////////////////////////////////////////////
//功能：绘制弧形前瞻
//输入：PROSPECT
//输出：弧形前瞻的线
//备注：
///////////////////////////////////////////
void draw_PROSPECT(void)
{
	int tnum;
	int y, x;   //  (x-93)*(x-93)+(y-119)*(y-119)=PROSPECT*PROSPECT
	for (x = 0; x <= CAMERA_W; x++)
	{
		tnum = ((int)PROSPECT * (int)PROSPECT) - (x - 70) * (x - 70);
		if (tnum >= 0)
		{
			y = -sqrt((float)tnum) + 119;
			IMG[y][x] = red;
		}
	}
}

////////////////////////////////////////////
//功能：确定赛道元素类型
//输入：
//输出：赛道元素类型
//备注：0为斑马线，1为直道，2为十字，3为右环岛，4为左环岛，5为入环岛，6位出环岛
///////////////////////////////////////////
uint8_t define_road_flag(void)
{
	uint8_t flag = 0;
	my_straight = check_straight();
	printf("my_straight:%d\n", my_straight);
	if (my_straight == 1)
	{
		straight_oridinary_two_line();
		return 1;
	}
	else
	{
		my_cross = check_cross();
		if (my_cross == 0)
		{
			my_fork = trident();
		}
		if (my_cross != 0)
		{
			return 2;
		}
		/*else if (youhuandao())
		{
			mood.circle = 1;
			return 3;
		}
		else if (zuohuandao())
		{
			mood.circle = 1;
			return 4;
		}
		else if (ruhuandao())
		{
			mood.ruhuandao = 1;
			return 5;
		}
		else if (chuhuandao())
		{
			mood.chuhuandao = 1;
			return 6;
		}*/
		else
		{
			return 10;
		}
	}
}

////////////////////////////////////////////
//功能：图像处理主程序
//输入：
//输出：
//备注：
///////////////////////////////////////////
void image_main()
{
	memset(right_side, CAMERA_W - 1, CAMERA_H);
	akward_area = 0;
	tell_all_white_flag = 0;
	uint8_t last_mid = 1, i = 0;
	float angle = 0;
	uint8_t width = 45;
	element_init();

	THRE();
	head_clear();

	inf.if_out = check_out();
	if (inf.if_out == 1)
		return;

	search_white_range();
	find_all_connect();
	find_road();
	/*到此处为止，我们已经得到了属于赛道的结构体数组my_road[CAMERA_H]*/
	find_x_right_left();

	x_left_straight = define_straight_line(x_left, NEAR_LINE - 10, FAR_LINE + 20);
	x_right_straight = define_straight_line(x_right, NEAR_LINE - 10, FAR_LINE + 20);
	bool my_roof = roof();

	for (uint8_t j = 1; j <= NEAR_LINE; j++)
	{
		left_line[j] = x_left[j];
		right_line[j] = x_right[j];
		//printf("x_left[%d]:%d,right_x_right[%d]:%d\n", j, x_left[j], j, x_right[j]);
		//printf("left_smooth[%d]:%d,right_smooth[%d]:%d\n", j, left_smooth[j], j, right_smooth[j]);
	}

	zebra_start_line = check_zebra();
	if (zebra_start_line != MISS)
	{
		inf.if_zebra = 1;
		clean_zebra();
		filter_two_line();
		get_mid_line();
		inf.my_angle = define_PROSPECT(PROSPECT);
		img_test();
		return;
	}

	inf.now_road_type = define_road_flag();
	printf("赛道类型为%d\n", inf.now_road_type);

	if (my_cross != 0)
	{
		cross_oridinary_two_line();
		if_connect();
	}
	if (my_fork == 1)
	{
		check_fork();
		for (uint8_t j = 1; j <= NEAR_LINE; j++)
		{
			left_line[j] = x_left[j];
			right_line[j] = x_right[j];
			//printf("x_left[%d]:%d,right_x_right[%d]:%d\n", j, x_left[j], j, x_right[j]);
			//printf("left_smooth[%d]:%d,right_smooth[%d]:%d\n", j, left_smooth[j], j, right_smooth[j]);
		}
	}

	filter_two_line();
	get_mid_line();

	img_test();


	//find_mid();

	//uint8_t break_mid = MISS;
	//for (uint8_t k = 1; k <= 199; k++)
	//{
	//	if ((int)mid_float[k].i == MISS)
	//	{
	//		i = (int)mid_float[k - 1].i;
	//		break_mid = (int)mid_float[k - 1].j;
	//		break;
	//	}
	//	last_mid_line[(int)mid_float[k].i] = (int)mid_float[k].j;
	//	//printf("i:%d  mid:%d\n", (int)mid_float[k].i, (int)mid_float[k].j);
	//}

	//for (i; i >= 1; i--)
	//{
	//	last_mid_line[i] = break_mid;
	//}

	//for (i = NEAR_LINE; i >= 1; i--)
	//{
	//	if (last_mid_line[i] != 0)
	//	{
	//		last_mid = last_mid_line[i];
	//		break;
	//	}
	//}
	//for (i; i <= NEAR_LINE; i++)
	//{
	//	last_mid_line[i] = last_mid;
	//}

	///*for (i = 113; i >= 1; i--)
	//{
	//	printf("i:%d  last:%d  \n", i,last_mid_line[i]);
	//}*/

	//if (0)
	//{
	//	angle = define_PROSPECT(35);
	//}
	//else
	//{
	//	angle = define_PROSPECT(PROSPECT);
	//}


	//inf.last_angle = inf.my_angle;
	//inf.my_angle = angle;

	//if (akward_area == 1)//若两帧之间差距大于50，或进入十字中间区域，则认为中线有问题
	//{
	//	inf.my_angle = inf.last_angle;
	//}
	//printf("偏差：%f\n", inf.my_angle);



}
